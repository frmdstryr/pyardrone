## Python ARDrone 2.0 library ##



### Setup ###

I ran this on ubuntu 14.10 and 15.04.

1. Install python
2. Install ipython, numpy, zmq, tornado, and opencv


### Joystick setup ###

1. Plugin in and run `ls /dev | grep hidraw` 
2. Change permissions on /dev/hidraw of the controller to 777 (or run as root)
3. You also may need to adjust the decoding in joystick.py
4. Update key bindings in ARDrone.handle_joystick for your joystick


### Video setup ###

Video will start about 10-30 seconds after running as it needs to wait for a full video frame

1. See video receiver in video.py for using opencv

### Running ###

1. Connect to the drone via wifi
2. Run python drone.py

