# -*- coding: utf-8 -*-
'''
Created on Apr 8, 2015

@author: jrm
'''
import numpy as np
import cv2

cap = cv2.VideoCapture(0)

kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
#fgbg = cv2.createBackgroundSubtractorGMG()
fgbg = cv2.BackgroundSubtractorMOG()

i=0
while(1):
    i+=1
    ret, frame = cap.read()

    fgmask = fgbg.apply(frame)
    fgmask = cv2.morphologyEx(fgmask, cv2.MORPH_OPEN, kernel)

    cv2.imshow('frame',fgmask)
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break
    
    if i&8:
        fgbg = cv2.BackgroundSubtractorMOG()
        i=0

cap.release()
cv2.destroyAllWindows()