# -*- coding: utf-8 -*-
'''
Created on Apr 19, 2015

@author: jrm
'''

import numpy as np
import cv2
import pyqtgraph as pg #from matplotlib import pyplot as plt

cap = cv2.VideoCapture(0)
fast = cv2.FastFeatureDetector()
last_kp = None
while(cap.isOpened()):
    ret, frame = cap.read()
    if ret==True:
        
        kp = fast.detect(frame,None)
        if last_kp:
            frame = cv2.drawKeypoints(frame,last_kp,color=(255,0,0))
        frame = cv2.drawKeypoints(frame,kp,color=(0,255,0))
        last_kp = kp

        cv2.imshow('frame',frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        break

# Release everything if job is finished
cap.release()
cv2.destroyAllWindows()
