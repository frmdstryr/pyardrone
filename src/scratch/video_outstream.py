import socket
import cv2
import numpy as np

from tornado.iostream import IOStream
from IPython.utils.traitlets import Unicode,Int

from IPython.config.configurable import LoggingConfigurable
from threading import Thread

class TrackedObject(object):
    """ Upon creation defines an object to start tracking """
    def __init__(self,frame,x,y,w,h):
        self.track_window = (x,y,w,h)
        self._first = True
        self.capture(frame)
    
    def capture(self,frame):
        # setup initial location of window
        r,h,c,w = self.track_window
        
        # set up the ROI for tracking
        roi = frame[r:r+h, c:c+w]
        hsv_roi =  cv2.cvtColor(roi, cv2.COLOR_BGR2HSV)
        mask = cv2.inRange(hsv_roi, np.array((0., 60.,32.)), np.array((180.,255.,255.)))
        
        self.roi_hist = cv2.calcHist([hsv_roi],[0],mask,[180],[0,180])
        cv2.normalize(self.roi_hist,self.roi_hist,0,255,cv2.NORM_MINMAX)
        
        # Setup the termination criteria, either 10 iteration or move by atleast 1 pt
        self.term_criteria = ( cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 1 )
        
    def update(self,frame,draw=True):
        """ Updates the position of the object based on the frame. """
        dst = cv2.calcBackProject(
          [cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)],
          [0],
          self.roi_hist,
          [0,180],
          1
        )
        
        # apply meanshift to get the new location
        ret, self.track_window = cv2.CamShift(dst, self.track_window, self.term_criteria)
        
        if draw:
            # Draw it on image
            #print track_window
            pts = cv2.cv.BoxPoints(ret)
            pts = np.int0(pts)
            cv2.polylines(frame,[pts],True, 255,2)
            
            if self._first:
                self._window = pts
            else:
                cv2.polylines(frame,[self._window],True, 10,2)
                
        
        self._first = False    
    


class MovingObjectTracker(LoggingConfigurable):
    
    video_stream = None
    tracked_object = None
    url = 0
    
    def run(self):
        try:
            self.video_stream.open(self.url)
            # take first frame of the video
            ret,frame = self.video_stream.read()
            
            self.tracked_object = TrackedObject(frame,x=300,y=100,w=10,h=100)
            
            i = 0
            while True:
                i+=1
                ret ,frame = self.video_stream.read()
                if not ret:
                    break
                
                self.tracked_object.update(frame)
                
                cv2.imshow('frame',frame)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
                if i & 8:
                    i=0
                    self.tracked_object = TrackedObject(frame,*self.tracked_object)
                
        finally:
            self.video_stream.release()
            
class VideoStreamHandler(LoggingConfigurable):
    ip = Unicode('192.168.1.1',config=True,help="drone ip")
    port = Int(5559,config=True,help="video stream port")
    
    def __init__(self,*args,**kwargs):
        LoggingConfigurable.__init__(self,*args,**kwargs)
        self.init_connection()
    
    @property
    def address(self):
        return (self.ip,self.port)
        
    def init_connection(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.setblocking(False)
        s.bind(('',0))
        self.stream = IOStream(s)
        self.stream.connect(address=self.address, callback=self.on_connect)
        self.stream.set_close_callback(self.on_close)
        
    def init_tracker(self):
        self.tracker = MovingObjectTracker(video_stream=cv2.VideoCapture())
    
    def on_connect(self):
        """ Sync with the drone 
        
        """
        self.log.info("Connected to video stream on %s"%(self.address,))
        self.stream.write("\x01\x00\x00\x00")
        self.init_tracker()
        self.tracker_thread = Thread(target=self.tracker.run)
    
    
    def on_close(self,data=None):
        """ Called when the stream is closed """
        self.log.warning("Disconnected from video stream %s"%(self.address,))
            

if __name__ == '__main__':
    tracker = MovingObjectTracker(video_stream=cv2.VideoCapture(0))
    tracker.run() 
    cv2.destroyAllWindows()