# -*- coding: utf-8 -*-
'''
Created on Apr 11, 2015

@author: jrm
'''
from ctypes import *
from IPython.config.configurable import LoggingConfigurable
from IPython.utils.traitlets import Instance,Unicode,Bool,Int
from tornado.iostream import PipeIOStream, _ERRNO_WOULDBLOCK
from tornado.ioloop import IOLoop
from multiprocessing.process import Process
from multiprocessing import Pipe
import traceback
import errno

class ConnectionIOStream(PipeIOStream):
    """ Works with multiprocessing.Pipe """
    def __init__(self, connection, *args, **kwargs):
        self.conn = connection
        super(ConnectionIOStream,self).__init__(connection.fileno(), *args, **kwargs)
        
    def fileno(self):
        return self.conn.fileno()
        
    def close_fd(self):
        self.conn.close()

    def write_to_fd(self, data):
        return self.conn.send(data)

    def read_from_fd(self):
        try:
            chunk = self.conn.recv_bytes(self.read_chunk_size)
        except (IOError, OSError) as e:
            if e.args[0] in _ERRNO_WOULDBLOCK:
                return None
            elif e.args[0] == errno.EBADF:
                # If the writing half of a pipe is closed, select will
                # report it as readable but reads will fail with EBADF.
                self.close(exc_info=True)
                return None
            else:
                raise
        if not chunk:
            self.close()
            return None
        return chunk
    

class JoystickData(Structure):
    _fields_ = [
        ('header',c_uint8),
        ('roll',c_uint8), # 0 left, 255 right
        ('pitch',c_uint8), # 0 front, 255 back
        ('yaw',c_uint8), # 0 left, 255 right
        ('throttle',c_uint8), # 0 forward, 255 back
        ('btns',c_uint8),
        ('joystick',c_uint8),
    ]#+['btn_%s'%i,c_uint16,1) for i in range(1,17)]
    
    def __repr__(self):
        return "Joystick(roll=%s,pitch=%s,yaw=%s,throttle=%s,btns=%s,joystick=%s)"%(self.roll,self.pitch,self.yaw,self.throttle,self.btns,self.joystick)

class JoystickStreamHandler(LoggingConfigurable):
    """ Reads input from a Saitek Cyborg 3D Rumble Force joystick  """
    device = Unicode('/dev/hidraw0',help="device path")
    stream = Instance(PipeIOStream)
    stream_reconnect = Bool(True)
    #loop = Instance(IOLoop,(),{})
    packet_size = Int(7)
    joystick = Instance(JoystickData)
    
    def __init__(self,*args,**kwargs):
        super(JoystickStreamHandler, self).__init__(*args,**kwargs)
        self.init_connection()
    
    def init_connection(self):
        
        def read_joystick(conn):
            with open(self.device,'rb') as dev:
                while True:
                    conn.send_bytes(dev.read(self.packet_size))
            conn.close()
        
        rx,tx = Pipe(duplex=False)
        self.proc = Process(target=read_joystick,args=(tx,))
        self.proc.daemon = True
        self.proc.start()
            
        self.stream = ConnectionIOStream(rx)
        self.stream.read_bytes(self.packet_size,self.on_input)
        
    
    def on_input(self,data):
        self.stream.read_bytes(self.packet_size,self.on_input)
        try:
            #self.log.error(data.encode('hex'))
            self.joystick = JoystickData.from_buffer_copy(data+'\x00')
            #self.log.warning(self.joystick)
        except:
            self.log.error(traceback.format_exc())
        
    
    def on_close(self,data=None):
        self.log.error("Input closed!")
        if self.stream_reconnect:
            self.init_connection()
        else:
            self.proc.kill()
            
   
if __name__ == '__main__':
    loop = IOLoop.instance()
    joystick = JoystickStreamHandler()#loop=loop)
    loop.start()
