'''
Created on Feb 27, 2015

@author: jrm
'''
import socket
from tornado.iostream import IOStream
from IPython.config.configurable import LoggingConfigurable
from IPython.utils.traitlets import Unicode,Int,Instance,Bool

class ARDroneStreamHandler(LoggingConfigurable):
    """ Base class for a stream handler"""
    ip = Unicode('192.168.1.1',config=True,help="drone ip")
    port = Int(0,config=True,help="drone port")
    stream_name = Unicode('drone stream')
    stream = Instance(IOStream)
    stream_reconnect = Bool(True,help="reconnect if the connection is dropped")
    
    def __init__(self,*args,**kwargs):
        LoggingConfigurable.__init__(self,*args,**kwargs)
        self.init_connection()
    
    @property
    def address(self):
        return (self.ip,self.port)
        
    def init_connection(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.setblocking(False)
        s.bind(('',0))
        self.stream = IOStream(s)
        self.stream.connect(address=self.address, callback=self.on_connect)
        self.stream.read_until_close(self.on_close, streaming_callback=self.on_packet)
        
    def on_connect(self):
        self.log.info("Connected to %s stream on %s"%(self.stream_name,self.address,))
        
    def on_packet(self,buf):
        pass
        
    def on_close(self,data=None):
        """ Called when the stream is closed """
        self.log.warning("Disconnected from %s stream %s"%(self.stream_name,self.address,))
        if self.stream_reconnect:
            self.init_connection()