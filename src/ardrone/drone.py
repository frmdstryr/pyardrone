'''
Created on Jan 28, 2015

@author: jrm
'''
import logging
from threading import Thread
from zmq.eventloop.ioloop import IOLoop

from IPython.config.application import Application
from IPython.utils.traitlets import Unicode,Int,Bool,Float

from ardrone.navdata import NavdataStreamHandler
from ardrone.control import ControlStreamHandler, ATStreamHandler
from ardrone.joystick import JoystickStreamHandler
from ardrone.video import VideoStreamHandler, VideoEncapsulation


class ARDrone(Application):
    """Controlling the AR.Drone is done through 3 main communication services.

    Controlling and configuring the drone is done by sending AT commands on UDP port 5556.
    The transmission latency of the control commands is critical to the user experience. Those
    commands are to be sent on a regular basis (usually 30 times per second). The list of available
    commands and their syntax is discussed in chapter 6.
    
    Information about the drone (like its status, its position, speed, engine rotation speed, etc.),
    called navdata, are sent by the drone to its client on UDP port 5554. These navdata also include
    tags detection information that can be used to create augmented reality games. They are sent
    approximatively 30 times per second.

    A video stream is sent by the AR.Drone to the client device on port 5555. Images from this video
    stream can be decoded using the codec included in this SDK. Its encoding format is discussed
    in section 7.2.

    A fourth communication channel, called control port, can be established on TCP port 5559 to
    transfer critical data, by opposition to the other data that can be lost with no dangerous effect.
    It is used to retrieve configuration data, and to acknowledge important information such as the
    sending of configuration information.
    """
    
    drone_ip = Unicode('192.168.1.1',config=True)
    ftp_port = Int(5551,config=True)
    auth_port = Int(5552,config=True)
    video_recorder_port = Int(5553,config=True)
    navdata_port = Int(5554,config=True)
    video_port = Int(5555,config=True)
    at_port = Int(5556,config=True)
    raw_capture_port = Int(5557,config=True)
    printf_port = Int(5558,config=True)
    control_port = Int(5559,config=True)
    
    console = Bool(False,help="Set to true if using in a console and it'll run the IOLoop in a thread (instead of blocking)")
    
    move_speed = Float(0.2,config=True,help="default move speed")
    turn_speed = Float(0.2,config=True,help="default turn speed")
    
    use_joystick = Bool(True,help="use joystick")
    
    def _log_level_default(self):
        return logging.DEBUG
    
    def _log_format_default(self):
        """override default log format to include time"""
        return u"%(asctime)s.%(msecs).03d | %(name)s | %(levelname)8s | %(message)s"

    def start(self):
        self.at_stream = ATStreamHandler(ip=self.drone_ip,port=self.at_port)
        self.navdata_stream = NavdataStreamHandler(ip=self.drone_ip,port=self.navdata_port)
        #self.control_stream = ControlStreamHandler(ip=self.drone_ip,port=self.control_port) # Not used 
        self.video_stream = VideoStreamHandler(ip=self.drone_ip,port=self.video_port)
        
        if self.use_joystick:
            self.joystick = JoystickStreamHandler()
            self.joystick.on_trait_change(self.handle_joystick, 'joystick')
            
        self.loop = IOLoop.instance()
        
        if self.video_stream:
            self.loop.add_callback(self.init_video)
        
        # So we can do stuff with navadata
        self.navdata_stream.on_trait_change(self.handle_navdata, 'navdata')
        
        if self.console:
            self.loop_thread = Thread(target=self.loop.start)
            self.loop_thread.daemon=True
            self.loop_thread.start()
        else:
            self.loop.start()
            
    def init_video(self):
        self.set_config("video:video_codec",VideoEncapsulation.CODEC_P264)
    
    def stop(self):
        self.loop.stop()
        
    def at(self,cmd,*args,**kwargs):
        """ Send an AT command to the drone. 
        @see ATStreamHandler.send() """
        return self.at_stream.send(cmd,*args,**kwargs)
        
    def takeoff(self):
        """ Send a command with this bit (9) set to 1 to make the drone take-off. This command
            should be repeated until the drone state in the navdata shows that drone actually
            took off. If no other command is supplied, the drone enters a hovering mode and
            stays still at approximately 1 meter above ground."""
        self.set_config("control:altitude_max", "20000")
        self.ref(True)
        
    def land(self,emergency=False):
        """ Send a command with this bit set to 0 to make the drone land. This command should
            be repeated until the drone state in the navdata shows that drone actually landed,
            and should be sent as a safety whenever an abnormal situatio is detected.."""
        self.ref(False, emergency)
        
    def hover(self):
        """Make the drone hover."""
        self.at("PCMD", 0, 0, 0, 0, 0)
        
    def move(self,roll=0.0,pitch=0.0,vert=0.0,yaw=0.0,flags=1):
        """
        @param lat: Move right (pos) or left (neg) 
        @param lng: Move forward (pos) or backward (neg)
        @param vert: Move up (pos) or down (neg)
        @param turn: Turn right (pos) or left (neg)
        """
        self.at("PCMD",flags,roll,pitch,vert,yaw)
        
    def move_left(self,speed=None,flags=1):
        """Make the drone move left."""
        speed = speed is None and self.move_speed or speed
        self.at("PCMD", flags, -speed, 0.0, 0.0, 0.0)

    def move_right(self,speed=None,flags=1):
        """Make the drone move right."""
        speed = speed is None and self.move_speed or speed
        self.at("PCMD", flags, speed, 0.0, 0.0, 0.0)

    def move_up(self,speed=None,flags=1):
        """Make the drone rise upwards."""
        speed = speed is None and self.move_speed or speed
        self.at("PCMD", flags, 0.0, 0.0, speed, 0.0)

    def move_down(self,speed=None,flags=1):
        """Make the drone decent downwards."""
        speed = speed is None and self.move_speed or speed
        self.at("PCMD", flags, 0.0, 0.0, -speed, 0.0)

    def move_forward(self,speed=None,flags=1):
        """Make the drone move forward."""
        speed = speed is None and self.move_speed or speed
        self.at("PCMD", flags, 0.0, -speed, 0.0, 0.0)

    def move_backward(self,speed=None,flags=1):
        """Make the drone move backwards."""
        speed = speed is None and self.move_speed or speed
        self.at("PCMD", flags, 0, speed, 0, 0)
        
    def turn_left(self,speed=None,flags=1):
        """Make the drone rotate left."""
        speed = speed is None and self.turn_speed or speed
        self.at("PCMD", flags, 0, 0, 0, -speed)

    def turn_right(self,speed=None,flags=1):
        """Make the drone rotate right."""
        speed = speed is None and self.turn_speed or speed
        self.at("PCMD", flags, 0, 0, 0, -speed)

    def reset(self):
        """Toggle the drone's emergency state."""
        self.ref(emergency=True)
        self.ref()
        self.ref(emergency=True)
        
    def ref(self,takeoff=False,emergency=False):
        p = 0b10001010101000000000000000000
        if takeoff:
            p += 0b1000000000
        if emergency:
            p += 0b0100000000
        self.at("REF",p)

    def trim(self):
        """Flat trim the drone."""
        self.at("FTRIM")
    
    def set_config(self,key,value):
        self.at("CONFIG",key,value)
    
    def handle_navdata(self,name,old,new):
        """ Called every time a navdata block is decoded (about every 50ms)
        
        Use this to map drone state to drone commands or any 
        state variables used by physics models etc...
         
        @param name: 'namvdata' 
        @param old: old navadata object
        @param new: new navdata object
        """
        pass
        
    def handle_joystick(self,name,old,new):
        """ Called every time new joystick input is received (about every 20ms)
        
        Use this to map joystick commands to drone commands. 
        
        @param name: 'joystick' 
        @param old: old joystick object
        @param new: new joystick object
        
        """
        joystick = new
        self.log.warning(joystick)
        if joystick.btns & 4: # btn 3
            self.takeoff()
        if joystick.btns & 8: # btn 4
            self.land()
        if joystick.btns & 64: # btn 7
            self.reset()
        if joystick.btns & 128:
            self.trim()
            
        # Hold trigger to move
        flags = (joystick.btns & 1 and 1) or 0
        
        # Adjust the speed based on how much
        pitch = 1-(255-joystick.pitch)/128.0
        yaw = 1-(255-joystick.yaw)/128.0
        roll = 1-(255-joystick.roll)/128.0
        vert = 1-(255-joystick.throttle)/128.0
        
        
        
        #vert = (joystick.joystick==64 and -1.0) or (joystick.joystick==0 and 1.0) or 0
        self.move(pitch=pitch,yaw=yaw,roll=roll,vert=vert,flags=flags)
                
        
        
if __name__ == '__main__':
    drone = ARDrone.instance()
    drone.start()
    