import socket
import traceback
import cv2
import numpy as np

from ctypes import *
from ardrone.utils import ARDroneStreamHandler,IOStream
from tornado.tcpserver import TCPServer
from IPython.utils.traitlets import Unicode,Int,List
from IPython.config.configurable import LoggingConfigurable
from multiprocessing.process import Process



class TrackedObject(object):
    """ Upon creation defines an object to start tracking """
    def __init__(self,frame,x,y,w,h):
        self.track_window = (x,y,w,h)
        self.capture(frame)
    
    def capture(self,frame):
        # setup initial location of window
        r,h,c,w = self.track_window
        
        # set up the ROI for tracking
        roi = frame[r:r+h, c:c+w]
        hsv_roi =  cv2.cvtColor(roi, cv2.COLOR_BGR2HSV)
        mask = cv2.inRange(hsv_roi, np.array((0., 60.,32.)), np.array((180.,255.,255.)))
        
        self.roi_hist = cv2.calcHist([hsv_roi],[0],mask,[180],[0,180])
        cv2.normalize(self.roi_hist,self.roi_hist,0,255,cv2.NORM_MINMAX)
        
        # Setup the termination criteria, either 10 iteration or move by atleast 1 pt
        self.term_criteria = ( cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 1 )
        
    def update(self,frame,draw=True):
        """ Updates the position of the object based on the frame. """
        dst = cv2.calcBackProject(
          [cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)],
          [0],
          self.roi_hist,
          [0,180],
          1
        )
        
        # apply meanshift to get the new location
        ret, self.track_window = cv2.CamShift(dst, self.track_window, self.term_criteria)
        if draw:
            # Draw it on image
            #print track_window
            pts = cv2.cv.BoxPoints(ret)
            pts = np.int0(pts)
            cv2.polylines(frame,[pts],True, 255,2)
            
            if self._first:
                self._initial_pts = pts
            else:
                cv2.polylines(frame,[self._initial_pts],True, 160,2)
    


class MovingObjectTracker(LoggingConfigurable):
    
    video_stream = None
    tracked_object = None
    url = 0
    _first = True
    
    def run(self):
        try:
            self.log.debug("Opening video stream %s"%self.url)
            self.video_stream.open(self.url)
            # take first frame of the video
            ret,frame = self.video_stream.read()
            
            self.tracked_object = TrackedObject(frame,window=(50,90,200,125))
            
            while True:
                ret ,frame = self.video_stream.read()
                if not ret:
                    break
                
                self.tracked_object.update(frame)
                
                cv2.imshow('frame',frame)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
        except:
            self.log.error(traceback.format_exc())
        finally:
            self.video_stream.release()
            
            
class VideoEncapsulation(Structure):
    PAVE_STREAM_ID_SUFFIX_MP4_360p = 0
    PAVE_STREAM_ID_SUFFIX_H264_360p = 1
    PAVE_STREAM_ID_SUFFIX_H264_720p = 2
    
    NULL_CODEC    = 0
    UVLC_CODEC    = 0x20       # codec_type value is used for START_CODE
    P264_CODEC    = 0x40
    MP4_360P_CODEC = 0x80
    H264_360P_CODEC = 0x81
    MP4_360P_H264_720P_CODEC = 0x82
    H264_720P_CODEC = 0x83
    MP4_360P_SLRS_CODEC = 0x84
    H264_360P_SLRS_CODEC = 0x85
    H264_720P_SLRS_CODEC = 0x86
    H264_AUTO_RESIZE_CODEC = 0x87    # resolution is automatically adjusted according to bitrate
    MP4_360P_H264_360P_CODEC = 0x88
    
    CODEC_UNKNNOWN = 0
    CODEC_VLIB = 1
    CODEC_P264 = 2
    CODEC_MPEG4_VISUAL = 3 
    CODEC_MPEG4_AVC = 4
    
    FRAME_TYPE_UNKNNOWN = 0
    FRAME_TYPE_IDR_FRAME = 1# , /* headers followed by I-frame */
    FRAME_TYPE_I_FRAME = 2
    FRAME_TYPE_P_FRAME = 3
    FRAME_TYPE_HEADERS = 4
    
    _fields_ = [
        #('signature',c_uint8 * 4), we skip this as we're reading until it's found
        ('version',c_uint8),
        ('video_codec',c_uint8),
        ('header_size',c_uint16),
        ('payload_size',c_uint32), 
        ('encoded_stream_width',c_uint16),
        ('encoded_stream_height',c_uint16),
        ('display_width',c_uint16),
        ('display_height',c_uint16),
        ('frame_number',c_uint32),
        ('timestamp',c_uint32),
        ('total_chunks',c_uint8),
        ('chunk_index',c_uint8),
        ('frame_type',c_uint8),
        ('control',c_uint8),
        ('stream_byte_position_lw',c_uint32),
        ('stream_byte_position_uw',c_uint32),
        ('stream_id',c_uint16),
        ('total_slices',c_uint8),
        ('slice_index',c_uint8),
        ('header1_size',c_uint8),
        ('header2_size',c_uint8),
        ('reserved2',c_uint8*2),
        ('advertized_size',c_uint32),
        ('reserved3',c_uint8*12),
        #('frame',VideoFrame),
    ]

class VideoStreamServer(TCPServer,LoggingConfigurable):
    streams = List()
    
    def handle_stream(self, stream, address):
        self.log.debug("Incoming video stream client from %s"%(address,))
        self.streams.append((stream,address))
        
    def write(self,data):
        for stream,address in self.streams:
            try:
                #self.log.debug("Writing: %s"%data.encode('hex'))
                stream.write(data)
            except:
                #self.log.error(traceback.format_exc())
                self.streams.remove((stream,address))
                self.log.debug("Video stream client %s closed"%(address,))
                
                

class VideoStreamReceiver(LoggingConfigurable):
    """ Reads decoded video stream from the ARDrone 
    
    
    """
    
    def run(self):
        try:
            while True:
                self.log.warning("Opening video stream")
                cap = cv2.VideoCapture()
                cap.set(cv2.cv.CV_CAP_PROP_FOURCC, cv2.cv.CV_FOURCC(*'AVC1'))#'A','V','C','1'))
                
                try:
                    self.log.debug("Waiting for first full video frame...")
                    #cap.open(0)
                    cap.open("tcp://127.0.0.1:6000")
                    
                    ret, frame = cap.read()
                    
                    # Determine the object to track
                    # 250,90,400,125
                    #x0, y0, x1, y1 = self.selection
                    # center point x,y
                    # and width,height
                    #try:
                    #    self.tracked_object = TrackedObject(frame,10,10,20,20)
                    #except:
                    #    self.tracked_object = None
                    
                    ret = True
                    while ret:
                        ret, frame = cap.read()
                        #try:
                        #if self.tracked_object:
                        #    self.tracked_object.update(frame)
                        #except:
                        #    self.log.error(traceback.format_exc())
                        cv2.imshow('frame', frame)
                        if cv2.waitKey(1):
                            break
                except:
                    self.log.error(traceback.format_exc())
                    #time.sleep(0.1)
                finally:
                    cap.release()
        finally:
            cv2.destroyAllWindows()

class VideoStreamHandler(ARDroneStreamHandler):
    stream_name = Unicode('video')
    ip = Unicode('192.168.1.2',config=True,help="drone ip")
    port = Int(5559,config=True,help="video stream port")
    
    pave_size = 64+8 # +4 for PaVE signature which is skipped as its used to trigger callback
    
    def __init__(self,*args,**kwargs):
        super(VideoStreamHandler, self).__init__(*args,**kwargs)
        self.init_broadcast()
        self.init_receiver()
        
    def init_connection(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setblocking(False)
        self.stream = IOStream(s)
        self.stream.connect(address=self.address, callback=self.on_connect)
        self.stream.set_close_callback(self.on_close)
        self.stream.read_until("PaVE", callback=self.on_first_packet)
    
    def init_broadcast(self):
        """ Start a broadcast stream of the unpacked video """
        self.broadcast = VideoStreamServer()
        self.broadcast.listen(6000)
        
    def init_receiver(self):
        r = VideoStreamReceiver()
        p = Process(target=r.run)
        p.daemon = True
        p.start()
    
    def init_tracker(self):
        self.log.debug("Starting object tracker...")
        #self.tracker = MovingObjectTracker(video_stream=cv2.VideoCapture(),url="udp://@%s:%i"%(self.ip,self.port))
    
    def on_first_packet(self,buf):
        """ Skip the first (it's empty) """
        self.stream.read_until("PaVE", callback=self.on_packet) # Keep reading
    
    def on_packet(self,buf):
        """ When we get a video packet strip the PaVE header and forward the video data """
        try:
            #self.log.debug(buf.encode('hex'))
            ve = VideoEncapsulation.from_buffer_copy(buf)
            #self.log.debug("version=%s,codec=%s, dims=(%s x %s), frame_num=%s, timestamp=%s, header_size=%s,payload_size=%s"%(ve.version,ve.video_codec,ve.display_width,ve.display_height,ve.frame_number,ve.timestamp,ve.header_size,ve.payload_size))
            
            # Write actual video data to the stream
            self.broadcast.write(buf[self.pave_size:self.pave_size+ve.payload_size]) 
        except:
            self.log.error(traceback.format_exc())
        finally:
            self.stream.read_until("PaVE", callback=self.on_packet) # Keep reading
            
    def on_broadcast_connect(self):
        self.log.debug("Broadcasting video on tcp://%s:%i"%self.broadcast_address)
        
    def on_broadcast_closed(self):
        self.log.debug("Broadcasting stopped...")
        
if __name__ == '__main__':
    r = VideoStreamReceiver()
    r.run()
    #
    #tracker = MovingObjectTracker(video_stream=cv2.VideoCapture(0))
    #tracker.run() 
    #cv2.destroyAllWindows()