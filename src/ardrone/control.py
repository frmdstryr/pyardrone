'''
Created on Feb 3, 2015

@author: jrm
'''
import socket
from ctypes import *
from IPython.utils.traitlets import Unicode,Int
from tornado.ioloop import PeriodicCallback
from ardrone.utils import ARDroneStreamHandler,IOStream
import struct

class ATStreamHandler(ARDroneStreamHandler):
    sequence = Int(1,help="sequence number to send with every command")
    stream_name = Unicode("AT stream")
    ip = Unicode('192.168.1.1',config=True,help="drone ip")
    port = Int(5556,config=True,help="at port")
    watchdog_interval = Int(200,help="time in ms to send the command")
        
    def init_connection(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.setblocking(False)
        self.stream = IOStream(s)
        self.stream.connect(address=self.address, callback=self.on_connect)
        self.stream.read_until("\n", callback=self.on_packet)
        self.stream.set_close_callback(self.on_close)
        
        # ping every 2 sec, timeout is 3
        self.ping_loop = PeriodicCallback(self.ping_drone,self.watchdog_interval)
        
    def on_connect(self):
        """ Sync with the drone """
        super(ATStreamHandler, self).on_connect()
        
        self.ping_loop.start()
        self.send("CONFIG","general:navdata_demo", "TRUE")
        self.send("CONFIG","general:num_version_config")
        self.get_drone_config()
    
    def on_packet(self,buf):
        """ Called when we get a packet"""
        self.stream.read_until("\n", callback=self.on_packet)
        # TODO: DECODE the data!!
        self.log.debug("AT packet: %s"%buf.encode('hex'))
        
    def send(self,cmd,*args,**kwargs):
        """ Send an AT command with the given arguments
        @see self._send_at """
        return self._send_at(cmd, args=args, **kwargs)
    
    def _send_at(self,cmd,args=[],repeat=1,verbose=True,callback=None):
        """ Send an AT command to the drone
        @param cmd: Command name to send
        @param args: Args to include with the command
        @param repeat: Number of times to repeat the command
        @param verbose: Set to true to log the command
        @param callback: Callback to invoke after the command is written each time 
        """
        args = ",".join(map(str,[self.sequence]+[self._format_arg(arg) for arg in args]))
        at_cmd = "AT*%s=%s\r" % (cmd,args)
        if verbose:
            self.log.info("AT -> Drone | %s"%(at_cmd,))
        self.stream.write(at_cmd.encode('utf-8'), callback)
        #self.stream.read_until("\n", callback=callback)
        self.sequence +=1
        
    def _format_arg(self,arg):
        """ Format arguments for passing as AT commands """
        if type(arg) == int:
            return arg
        elif type(arg) == float:
            # Convert to float to int
            #return struct.unpack('i', struct.pack('f', arg))[0]
            return cast((c_float*1)(arg),POINTER(c_int)).contents.value
        elif type(arg) == str:
            return '"%s"'%arg
        return arg
        
    def ping_drone(self):
        """ Required to keep the connection alive"""
        self.send("COMWDG",verbose=False)
        
    def get_drone_config(self):
        CFG_GET_CONTROL_MODE = 4
        self.send("CTRL",CFG_GET_CONTROL_MODE,0,repeat=100)
        
    
    def on_close(self,data=None):
        """ Called when the stream is closed """
        super(ATStreamHandler, self).on_close()
        self.ping_loop.stop()
        self.sequence = 1
        
        
class ControlStreamHandler(ARDroneStreamHandler):
    stream_name = Unicode("Control stream")
    ip = Unicode('192.168.1.1',config=True,help="drone ip")
    port = Int(5559,config=True,help="control port")
    
    def init_connection(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setblocking(False)
        self.stream = IOStream(s)
        self.stream.connect(address=self.address, callback=self.on_connect)
        self.stream.read_until("\n", callback=self.on_packet)
        self.stream.set_close_callback(self.on_close)
    
    def on_connect(self):
        """ Sync with the drone """
        super(ControlStreamHandler, self).on_connect()
        self.stream.write("\x01\x00\x00\x00")
    
    def on_packet(self,buf):
        """ Called when we get a packet"""
        self.stream.read_until("\n", callback=self.on_packet)
        # TODO: DECODE the data!!
        self.log.debug("Control packet: %s"%buf.encode('hex'))
        