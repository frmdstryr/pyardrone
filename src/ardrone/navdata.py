'''
Created on Jan 28, 2015

@author: jrm
'''
import socket
from ctypes import *
from IPython.utils.traitlets import Unicode,Int, Instance
import traceback
from ardrone.control import ATStreamHandler
from ardrone.utils import ARDroneStreamHandler,IOStream

class Matrix33(Structure):
    _fields_ = [("m%i%i"%(i,j),c_float) for i in range(3) for j in range(3)]
    def __repr__(self):
    	return "[[%f %f %f][%f %f %f][%f %f %f]]"%[getattr(self,"m%i%i"%(i,j)) for i in range(3) for j in range(3)]

class Vector3(Structure):
    _fields_ = [('x',c_float),('y',c_float),('z',c_float)]

class Vector31(Union):
    _anonymous_ = ("p",)
    _fields_ = [('v',c_float * 3),('p',Vector3)]

class NavdataOption(Structure):
    _fields_ = [
        ('tag',c_uint16),('size',c_uint16),
        ('data',POINTER(c_uint8))
    ]
    
class Navdata(Structure):
    _fields_ = [
        ('header',c_uint32),
        ('ardrone_state',c_uint32), # TODO: this can be broken down bitwise
        ('sequence',c_uint32),
        ('vision_defined',c_bool),
        ('options',NavdataOption*1)
    ]
    
    
    ARDRONE_FLY_MASK            = 0  #!< FLY MASK : (0) ardrone is landed (1) ardrone is flying */
    ARDRONE_VIDEO_MASK          = 1  #!< VIDEO MASK : (0) video disable (1) video enable */
    ARDRONE_VISION_MASK         = 2  #!< VISION MASK : (0) vision disable (1) vision enable */
    ARDRONE_CONTROL_MASK        = 3  #!< CONTROL ALGO : (0) euler angles control (1) angular speed control */
    ARDRONE_ALTITUDE_MASK       = 4  #!< ALTITUDE CONTROL ALGO : (0) altitude control inactive (1) altitude control active */
    ARDRONE_USER_FEEDBACK_START = 5  #!< USER feedback : Start button state */
    ARDRONE_COMMAND_MASK        = 6  #!< Control command ACK : (0) None (1) one received */
    ARDRONE_CAMERA_MASK         = 7  #!< CAMERA MASK : (0) camera not ready (1) Camera ready */
    ARDRONE_TRAVELLING_MASK     = 8  #!< Travelling mask : (0) disable (1) enable */
    ARDRONE_USB_MASK            = 9  #!< USB key : (0) usb key not ready (1) usb key ready */
    ARDRONE_NAVDATA_DEMO_MASK   = 10 #!< Navdata demo : (0) All navdata (1) only navdata demo */
    ARDRONE_NAVDATA_BOOTSTRAP   = 11 #!< Navdata bootstrap : (0) options sent in all or demo mode (1) no navdata options sent */
    ARDRONE_MOTORS_MASK         = 12 #!< Motors status : (0) Ok (1) Motors problem */
    ARDRONE_COM_LOST_MASK       = 13 #!< Communication Lost : (1) com problem (0) Com is ok */
    ARDRONE_SOFTWARE_FAULT      = 14 #!< Software fault detected - user should land as quick as possible (1) */
    ARDRONE_VBAT_LOW            = 15 #!< VBat low : (1) too low (0) Ok */
    ARDRONE_USER_EL             = 16 #!< User Emergency Landing : (1) User EL is ON (0) User EL is OFF*/
    ARDRONE_TIMER_ELAPSED       = 17 #!< Timer elapsed : (1) elapsed (0) not elapsed */
    ARDRONE_MAGNETO_NEEDS_CALIB = 18 #!< Magnetometer calibration state : (0) Ok no calibration needed (1) not ok calibration needed */
    ARDRONE_ANGLES_OUT_OF_RANGE = 19 #!< Angles : (0) Ok (1) out of range */
    ARDRONE_WIND_MASK           = 20 #!< WIND MASK: (0) ok (1) Too much wind */
    ARDRONE_ULTRASOUND_MASK     = 21 #!< Ultrasonic sensor : (0) Ok (1) deaf */
    ARDRONE_CUTOUT_MASK         = 22 #!< Cutout system detection : (0) Not detected (1) detected */
    ARDRONE_PIC_VERSION_MASK    = 23 #!< PIC Version number OK : (0) a bad version number (1) version number is OK */
    ARDRONE_ATCODEC_THREAD_ON   = 24 #!< ATCodec thread ON : (0) thread OFF (1) thread ON */
    ARDRONE_NAVDATA_THREAD_ON   = 25 #!< Navdata thread ON : (0) thread OFF (1) thread ON */
    ARDRONE_VIDEO_THREAD_ON     = 26 #!< Video thread ON : (0) thread OFF (1) thread ON */
    ARDRONE_ACQ_THREAD_ON       = 27 #!< Acquisition thread ON : (0) thread OFF (1) thread ON */
    ARDRONE_CTRL_WATCHDOG_MASK  = 28 #!< CTRL watchdog : (1) delay in control execution (> 5ms) (0) control is well scheduled */
    ARDRONE_ADC_WATCHDOG_MASK   = 29 #!< ADC Watchdog : (1) delay in uart2 dsr (> 5ms) (0) uart2 is good */
    ARDRONE_COM_WATCHDOG_MASK   = 30 #!< Communication Watchdog : (1) com problem (0) Com is ok */
    ARDRONE_EMERGENCY_MASK      = 31  #!< Emergency landing : (0) no emergency (1) emergency */
    
    
    def get_ardrone_state(self,mask):
        return bool((self.ardrone_state << mask) & 1)
    
    

    
class NavdataDemo(Structure):
    _fields_ = [
        ('tag',c_uint16),('size',c_uint16),
        ('ctrl_state',c_uint32),
        ('vbat_flying_percentage',c_uint32),
        ('theta',c_float),
        ('phi',c_float),
        ('psi',c_float),
        ('altitude',c_int32),
        ('vx',c_float),
        ('vy',c_float),
        ('vz',c_float),
        ('num_frames',c_uint32),
        ('detection_camera_rot',Matrix33),
        ('detection_camera_trans',Vector31),
        ('detection_camera_type',c_uint32),
        ('drone_camera_rot',Matrix33),
        ('drone_camera_trans',Vector31)
    ]
    
    ctrl_state_landed = 0
    ctrl_state_flying = 1
    ctrl_state_hovering = 2
    
    
    def __repr__(self):
        return "NavdataDemo(tag=%s,size=%s,ctrl_state=%s,vbat_flying_percentage=%s,theta=%s,phi=%s,psi=%s,altitude=%s,vx=%s,vy=%s,vz=%s,num_frames=%s"%(
                 self.tag,self.size,self.ctrl_state,self.vbat_flying_percentage,self.theta,self.phi,self.psi,
                 self.altitude,self.vx,self.vy,self.vz,self.num_frames,)

class NavdataCks(Structure):
    """ Checksum for all navdatas (including options) """
    _fields_ = [('tag',c_uint16),('size',c_uint16),('cks',c_uint32)]

class NavdataTime(Structure):
    """ < 32 bit value where the 11 most significant bits represents the seconds, and the 21 least significant bits are the microseconds """
    _fields_ = [('tag',c_uint16),('size',c_uint16),('cks',c_uint32)]

class NavdataRawMeasures(Structure):
    _fields_ = [
        ('tag',c_uint16),('size',c_uint16),
        ('raw_accs',c_uint16*3),
        ('raw_gyros',c_int16*3),
        ('raw_gyros_100',c_int16*2),
        ('vbat_raw',c_uint32),
        ('us_debut_echo',c_uint16),
        ('us_fin_echo',c_uint16),
        ('us_association_echo',c_uint16),
        ('us_distance_echo',c_uint16),
        ('us_courbe_temps',c_uint16),
        ('us_courbe_valeur',c_uint16),
        ('us_courbe_ref',c_uint16),
        ('flag_echo_ini',c_uint16),
        ('nb_echo',c_uint16),
        ('sum_echo',c_uint32),
        ('alt_temp_raw',c_int32),
        ('gradient',c_int16),
    ]

class NavdataPressureRaw(Structure):
    _fields_ = [
        ('tag',c_uint16),('size',c_uint16),
        ('up',c_int32),
        ('ut',c_int16),
        ('temperature_meas',c_int32),
        ('pressure_meas',c_int32),
    ]


class NavdataMagento(Structure):
    _fields_ = [
        ('tag',c_uint16),('size',c_uint16),
        ('mx',c_int16),('my',c_int16),('mz',c_int16),
        ('magento_raw',Vector31),
        ('magento_rectified',Vector31),
        ('magento_offset',Vector31),
        ('heading_unwrapped',c_float),
        ('heading_gyro_unwrapped',c_float),
        ('heading_fusion_unwrapped',c_float),
        ('magento_calibration_ok',c_char),
        ('magento_state',c_uint32),
        ('magento_radius',c_float),
        ('error_mean',c_float),
        ('error_var',c_float),
    ]

class NavdataWindSpeed(Structure):
    _fields_ = [
        ('tag',c_uint16),('size',c_uint16),
        ('wind_speed',c_float),
        ('wind_angle',c_float),
        ('wind_compensation_theta',c_float),
        ('wind_compensation_phi',c_float),
        ('state_x1',c_float),
        ('state_x2',c_float),
        ('state_x3',c_float),
        ('state_x4',c_float),
        ('state_x5',c_float),
        ('state_x6',c_float),
        ('magneto_debug1',c_float),
        ('magneto_debug2',c_float),
        ('magneto_debug3',c_float),
    ]

class NavdataKalmanPressure(Structure):
    _fields_ = [
        ('tag',c_uint16),('size',c_uint16),
        ('offset_pressure',c_float),
        ('est_z',c_float),
        ('est_zdot',c_float),
        ('est_bias_PWM',c_float),
        ('est_biais_pression',c_float),
        ('offset_US',c_float),
        ('wind_angle',c_float),
        ('prediction_US',c_float),
        ('cov_alt',c_float),
        ('cov_PWM',c_float),
        ('cov_vitesse',c_float),
        ('bool_effet_sol',c_bool),
        ('somme_inno',c_float),
        ('flag_rejet_US',c_bool),
        ('u_multisinus',c_float),
        ('gaz_altitude',c_float),
        ('flag_multisinus',c_bool),
        ('flag_multisinus_debug',c_bool),
    ]


class NavdataPhysMeasures(Structure):
    pass

class NavdataGyrosOffsets(Structure):
    pass

class NavdataEulerAngles(Structure):
    pass

class NavdataReferences(Structure):
    pass

class NavdataTrims(Structure):
    pass

class NavdataRcReferences(Structure):
    pass

class NavdataPwm(Structure):
    pass

class NavdataAltitude(Structure):
    pass

class NavdataVisionRaw(Structure):
    pass

class NavdataVisionOf(Structure):
    _fields_ = [
        ('tag',c_uint16),('size',c_uint16),
        ('of_dx',c_float*5),
        ('of_dy',c_float*5),
    ]

class NavdataVision(Structure):
    pass

class NavdataVisionPerf(Structure):
    pass

class NavdataTrackersSend(Structure):
    pass

class NavdataVisionDetect(Structure):
    pass

class NavdataWatchdog(Structure):
    _fields_ = [
        ('tag',c_uint16),('size',c_uint16),
        ('watchdog',c_int32),
    ]

class NavdataADCDataFrame(Structure):
    _fields_ = [
        ('tag',c_uint16),('size',c_uint16),
        ('version',c_uint32),
        ('data_frame',c_uint8*32),
    ]

class NavdataVideoStream(Structure):
    _fields_ = [
        ('tag',c_uint16),('size',c_uint16),
        ('quant',c_uint8),
        ('frame_size',c_uint32),
        ('frame_number',c_uint32),
        ('atcmd_ref_seq',c_uint32),
        ('atcmd_mean_ref_gap',c_uint32),
        ('atcmd_var_ref_gap',c_float),
        ('atcmd_ref_quality',c_uint32),
        ('out_bitrate',c_uint32),
        ('desired_bitrate',c_uint32),
        ('data1',c_int32),
        ('data2',c_int32),
        ('data3',c_int32),
        ('data4',c_int32),
        ('data5',c_int32),
        ('tcp_queue_level',c_uint32),
        ('fifo_queue_level',c_uint32),
    ]

class NavdataHDVideoStream(Structure):
    _fields_ = [
        ('tag',c_uint16),('size',c_uint16),
        ('hdvideo_state',c_uint32),
        ('storage_fifo_nb_packets',c_uint32),
        ('storage_fifo_size',c_uint32),
        ('usbkey_size',c_uint32),
        ('usbkey_freespace',c_uint32),
        ('frame_number',c_uint32),
        ('usbkey_remaining_time',c_uint32),
    ]

class NavdataGames(Structure):
    _fields_ = [
        ('tag',c_uint16),('size',c_uint16),
        ('double_tap_counter',c_uint32),
        ('finish_line_counter',c_uint32),
    ]

class NavdataWifi(Structure):
    _fields_ = [(
        'tag',c_uint16),('size',c_uint16),('link_quality',c_uint32)
    ]
    
class NavdataZimmu3000(Structure):
	pass
    
class NavdataUnpacked(Structure):
	_fields_ = [
		('sequence',c_uint32),
		('ardrone_state',c_uint32),
		('vision_defined',c_bool),
		('last_navdata_refresh',c_uint32),
		('demo',NavdataDemo),
		('raw_measures',NavdataRawMeasures),
		('phys_measures',NavdataPhysMeasures),
		('gyros_offsets',NavdataGyrosOffsets),
		('euler_angles',NavdataEulerAngles),
		('references',NavdataReferences),
		('trims',NavdataTrims),
		('rc_references',NavdataRcReferences),
		('pwm',NavdataPwm),
		('altitude',NavdataAltitude),
		('vision_raw',NavdataVisionRaw),
		('vision_of',NavdataVisionOf),
		('vision_perf',NavdataVisionPerf),
		('trackers_send',NavdataTrackersSend),
		('vision_detect',NavdataVisionDetect),
		('watchdog',NavdataWatchdog),
		('adc_data_frame',NavdataADCDataFrame),
		('video_stream',NavdataVideoStream),
		('games',NavdataGames),
		('pressure_raw',NavdataPressureRaw),
		('magento',NavdataMagento),
		('wind_speed',NavdataWindSpeed),
		('kalman_pressure',NavdataKalmanPressure),
		('hdvideo_stream',NavdataHDVideoStream),
		('wifi',NavdataWifi),
		('zimmu_3000',NavdataZimmu3000),
		('checksum',NavdataCks)
	]

class NavdataStreamHandler(ARDroneStreamHandler):
    NAVDATA_HEADER = 0x55667788
    NAVDATA_MAX_SIZE = 4096
    
    stream_name = Unicode('navdata')
    at_stream = Instance(ATStreamHandler)
    ip = Unicode('192.168.1.1',config=True,help="drone ip")
    port = Int(5554,config=True,help="navdata port")
    navdata = Instance(Navdata)
    
    navdata_options = {
        0:NavdataDemo,
        1:NavdataRawMeasures,
        2:NavdataPhysMeasures,
        3:NavdataGyrosOffsets,
        4:NavdataEulerAngles,
        5:NavdataReferences,
        6:NavdataTrims,
        7:NavdataRcReferences,
        8:NavdataPwm,
        9:NavdataAltitude,
        10:NavdataVisionRaw,
        11:NavdataVisionOf,
        12:NavdataVision,
        13:NavdataVisionPerf,
        14:NavdataTrackersSend,
        15:NavdataVisionDetect,
        16:NavdataWatchdog,
        17:NavdataADCDataFrame,  
        18:NavdataVideoStream,
        19:NavdataGames,
        20:NavdataPressureRaw,
        21:NavdataMagento,
        22:NavdataWindSpeed,
        23:NavdataKalmanPressure,
        24:NavdataHDVideoStream,
        25:NavdataWifi,
    }
    
    def init_connection(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.setblocking(False)
        s.bind(('',0))
        self.stream = IOStream(s)
        self.stream.read_chunk_size = self.NAVDATA_MAX_SIZE
        self.stream.connect(address=self.address, callback=self.on_connect)
        self.stream.read_until_close(self.on_close, streaming_callback=self.on_packet)
    
    def on_connect(self):
        """ Sync with the drone 
        // 1 -> Unicast
        // 2 -> Multicast
        """
        super(NavdataStreamHandler, self).on_connect()
        self.stream.write("\x01\x00\x00\x00")
    
    def on_packet(self,buf):
        """ Called when we get a packet"""
        try:
            # TODO: DECODE the data!!
            #self.log.debug("Navdata packet: %s"%buf.encode('hex'))
            
            # Decode the base packet
            offset = 0
            navdata = Navdata.from_buffer_copy(buf,offset)
            offset+=sizeof(navdata)
            if navdata.header != self.NAVDATA_HEADER:
                self.log.warning("Invalid packet from %s stream!"%self.stream_name)
                return
            elif navdata.get_ardrone_state(Navdata.ARDRONE_COM_WATCHDOG_MASK):
                self.log.warning("Communication watchdog is not ok!")
                #self.at_stream.send_at()
                #return
            
            # Read our option
            option = navdata.options[0]
            if option.size==0:
                return # nothing to unpack
            
            # Unpack the given option 
            cls = self.navdata_options[option.tag]
            data = cls.from_buffer_copy(buf,offset-sizeof(NavdataOption)) # structure starts at Navdata.option address
            self.on_packet_decoded(data)
            
        except:
            self.log.error(traceback.format_exc())
            
    def on_packet_decoded(self,data):
        self.log.debug(data)
        pass
    
    